//
//  ViewController.m
//  GitTest
//
//  Created by 王佩 on 16/2/25.
//  Copyright © 2016年 lanou3g.com 蓝鸥科技. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor redColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
